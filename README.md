# live-projects

_My (current) projects_

**Demos**

- [NextJS-Strapi Login](https://gitlab.com/michaelmarid/nextjs-strapi-login)


**Web Apps**

- [TEA ISTH (IORP)](https://teaisthmis.opf.gr/)

- [TEA SOEL (IORP)](https://teasoelmis.opf.gr/)

- [TEA BETA CAE (IORP)](https://teabetacaemis.opf.gr/)

- [Calculation of Main Pension for Freelancers](https://prudential-group.gr/mainpension/)

- [Future Provision](https://opf.gr/estimator/estimator.php)

- [Registration Form Example (TEAISTH)](https://register.opf.gr/index.php?opf=teaisth)



**Mobile**

- [OPF - Google Play Version](https://play.google.com/store/apps/details?id=com.prudentialgroup.opf&hl=en)

- [OPF - App Store Version](https://apps.apple.com/gr/app/occupational-pension-fund/id1479563922?ign-mpt=uo%3D4=)



**Static Sites/Blogs**

- [Prudential PC](https://prudential-group.gr/)

- [EEEKE](http://eeeke.gr/)

- [Antigoni Voutou Dance School](https://antigoni-voutou.gr/)

- [Infegreece (WP)](http://infegreece.gr/)

- [Infenetwork (WP)](https://infenetwork.net/)


**Private**

- E-Voting System (Fullstack React/GraphQL/PostreSQL)
- Professional CRM (with ticketing system) (Fullstack React/GraphQL/PostreSQL)




